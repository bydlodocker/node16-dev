FROM dobrebydlo/node16:16.20.1-a138-0.0.2

ARG UPASSWORD=password
ARG ROOT_PASSWORD=password

ENV SHELL=/bin/bash

USER root

RUN set -ex && \
    apk add -q --upgrade --no-cache --virtual .dev-deps \
        shadow \
        su-exec \
        sudo \
    && \
    rm -rf /var/cache/apk/* && \
    usermod -s ${SHELL} ${UNAME} && \
    usermod -s ${SHELL} root && \
    usermod -s ${SHELL} operator && \
    echo "root:${ROOT_PASSWORD}" | chpasswd && \
    echo "${UNAME}:${UPASSWORD}" | chpasswd && \
    printf "\nALL ALL=(ALL) NOPASSWD: ALL\n" >> /etc/sudoers

USER ${UNAME}
